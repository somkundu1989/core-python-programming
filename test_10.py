#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'timeConversion' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING s as parameter.
#

def timeConversion(s):
    s_list = s.split(':')    
    if s[-2:] == 'PM' :

        if int(s_list[0]) == 12 :
            s_list[0] = '12'
        else :
            s_list[0] = int(s_list[0]) + 12
            s_list[0] = str(s_list[0])
        s_list[2] = str(s_list[2]).replace('PM', '')
    else :
        if int(s_list[0]) == 12 :
            s_list[0] = '00'
        s_list[2] = str(s_list[2]).replace('AM', '')
    return ":".join(s_list)


stringData = input("Enter the Time string :: ") 
print(timeConversion(stringData))

