
# // Write a function to check whether two given strings are anagram of each other or not. 

# // An anagram of a string is another string that contains the same characters. Only the order of characters can be different. 
# // Means, Anagram is a word, phrase, or name formed by rearranging the letters of another. 

# // Anagram Examples:
# // Dormitory & Dirty room
# // Schoolmaster & The classroom
# // Listen & Silent
# // Astronomer & Moon starer
# // The eyes & They see
# // A gentleman & Elegant man
# // Funeral & Real fun
# // Pre-conditions:
# // You can use any language. 
# // The program must be generic.
# // The function and logic must be well written. Accepting inputs from the user is not important.
 

def stripStr(string) :
	return  "".join(string.split())

def checkAnagram(string1, string2) :
	returnStr = "not anagram"
	tempStr = string1
	if len(stripStr(string1)) == len(stripStr(string2)) :
		matchedPosition = []
		for eachChar in stripStr(string1):
			position = string2.index(eachChar)
			if position < 0 :
				return "not anagram"
			matchedPosition.append(position)
			tempStr = tempStr.replace(eachChar,'')
		if len(matchedPosition) == len(stripStr(string1)) :
			returnStr = " is Anagram"
	return returnStr

string1 = input("Enter the 1st string :: ") 
string2 = input("Enter the 2nd string :: ") 

resultStr = checkAnagram(string1, string2)
print(resultStr)


