# palindrome 


def checkPalindrome(originalString) :
	checkStatus = True;
	defaultIndex = 0
	for reverseEachChar in originalString[::-1] :
		if reverseEachChar != originalString[defaultIndex] :
			return False
		defaultIndex += 1
	return True


stringData = input("Enter the string :: ") 
checkStatus = checkPalindrome(stringData)
if checkStatus == True :
	print (stringData + ' is a palindrome string.')
else :
	print (stringData + ' is not a palindrome string.')