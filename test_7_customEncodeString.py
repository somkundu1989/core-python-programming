
# // Run-length encoding is a fast and simple method of encoding strings. 
# // The basic idea is to represent repeated successive characters as a single count and character. 
# // For example, the string "AAAABBBCCDAA" would be encoded as "4A3B2C1D2A".
# // Implement run-length encoding and decoding. 
# // You can assume the string to be encoded has no digits and consists solely of alphabetic characters. 
# // You can assume the string to be decoded is valid.

def customEncodeString (originalString) :
    tempStr = '';
    tempChar = originalString[0];
    tempCharCount = 0;

    for eachChar in originalString :
        if tempChar == eachChar :
            tempCharCount += 1
        else :
            tempStr += str(tempCharCount) + tempChar
            tempChar = eachChar
            tempCharCount = 1
    tempStr += str(tempCharCount) + tempChar
    return tempStr

    
#AAAABBBCCDAABBA
stringData = input("Enter the string :: ") 
print(customEncodeString(stringData))

#4A3B2C1D2A2B1A
