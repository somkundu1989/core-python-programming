import requests
import json


def clean_data(resultData):
  finalArr = dict()
  for keyIndex, eachItem in resultData.items():
    # finalArr[keyIndex]=type(eachItem)

    if (isinstance(eachItem, int) or isinstance(eachItem, str)):
      finalArr[keyIndex] = eachItem
    
    if (isinstance(eachItem, dict)):
      tmpArr = dict()
      for key, dataVal in eachItem.items():
        tmpArr[key] = dataVal
      finalArr[keyIndex] = tmpArr

    if (isinstance(eachItem, list)):
      tmpArr = []
      for dataVal in eachItem:
        tmpArr.append(dataVal)
      finalArr[keyIndex] = tmpArr




  print (finalArr);
  return 



r = requests.get('https://coderbyte.com/api/challenges/json/json-cleaning')
resultData = r.json()
clean_data(resultData)



